
<div class="item">
    <h1>VCF (Variant Call Format) specification</h1>
    <ul>
    <li><a href="http://www.1000genomes.org/wiki/Analysis/Variant%20Call%20Format/vcf-variant-call-format-version-40">VCF format v4.0</a></li>
    <li><a href="http://www.1000genomes.org/wiki/Analysis/Variant%20Call%20Format/VCF%20%28Variant%20Call%20Format%29%20version%204.0/encoding-structural-variants">VCF and structural variants</a></li>
    <li><a href="http://www.1000genomes.org/wiki/Analysis/Variant%20Call%20Format/vcf-variant-call-format-version-41">VCF format v4.1</a></li>
    </ul>
    <br>
    <ul>
    <li><a href="VCF-poster.pdf">VCF poster</a></li>
    <li><A href="https://lists.sourceforge.net/lists/listinfo/vcftools-spec">VCFtools-spec</a> - Low traffic mailing list intended for VCF format related
        discussions, such as clarifications of the current format version or
        proposals of changes to the specification. 
        </li>
    </ul>
    <br>
    <ul>
    <li><a href="bcf.pdf">Binary Call Format (BCF)</a> - Binary format used by 
        <a href="http://samtools.sourceforge.net/">samtools</a>/<a href="http://samtools.sourceforge.net/mpileup.shtml">bcftools</a>
        for efficient storing and parsing of genotype likelihoods. There are currently
        no plans to develop it beyond samtools, but we post it here anyway because of its general usefulness.
        </li>
    </ul>
</div>

