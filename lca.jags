#    Copyright 2012 Aaron J. Mackey

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

model {

  theta ~ dbeta(1,2);

  for (i in 1:N) {
    fp[i] ~ dbeta(1,2);
    fn[i] ~ dbeta(1,2);
    tp[i] <- 1-fn[i];
    tn[i] <- 1-fp[i];
  }

  # write out a string for the code to calculate marginal probabilities, 
  # and posterior probabilities, then evaluate R code 

  cmdstring = paste( "for (j in 1:M) {\nmargprobs[j] <- theta * \n" )
  for (i in 1:N){ 
     if ( i == numberOfSnpCallSets ){
       jagCode = paste(jagCode, "\t(fn[", i, "]^(1-x[j,", i, "])) * (tp[", i, "]^x[j,", i, "]) \n", sep="") 
     }
     else {
       jagCode = paste(jagCode, "\t(fn[", i, "]^(1-x[j,", i, "])) * (tp[", i, "]^x[j,", i, "]) *\n", sep="") 
     }
  }
  cmdstring = paste(cmdstring, "\n + (1-theta) * \n")
  for (i in 1:N){ 
     if ( i == N ){
          cmdstring = paste(cmdstring, "(tn[", i, "]^(1-x[j,", i, "])) * (fp[", i, "]^x[j,", i, "]);\n", sep="")
     }
     else {
          cmdstring = paste(cmdstring, "(tn[", i, "]^(1-x[j,", i, "])) * (fp[", i, "]^x[j,", i, "]) *\n", sep="")
     }
  }

  cmdstring = paste(cmdstring, "postprobs[j] <- theta * ", sep="")

  for (i in 1:N){ 
     if ( i == N ){
          cmdstring = paste(cmdstring, "(fn[", i, "]^(1-x[j,", i, "])) * (tp[", i, "]^x[j,", i, "])\n", sep="")
     }
     else {
          cmdstring = paste(cmdstring, "(fn[", i, "]^(1-x[j,", i, "])) * (tp[", i, "]^x[j,", i, "]) *\n", sep="")
     }
  }
  cmdstring = paste(cmdstring, "}\n", sep="")
  eval( parse(text = cmdstring ) )

  counts ~ dmulti(margprobs, total);
}
